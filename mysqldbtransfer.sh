#!/bin/bash
# MySQL DB Transfer skipping tables
# Author: Lucas Teles - lucas@grupow.com

# @todo: configure 'save' parameter to save db to file or

# FROM
from_user=""
from_password=""
from_host=""
from_database=""

# TO
to_user=""
to_password=""
to_host=""
to_database=""

# PATH TO SAVE GZIPED DB
outpath="database"

# SKIP TABLES
skip_tables=(
	configuration
)

ignored_tables_string=''
for table in "${skip_tables[@]}"
do :
   ignored_tables_string+=" --ignore-table=${from_database}.${table}"
done

umask 177

echo "Dumping content"
mysqldump \
	--host=${from_host} \
	--user=${from_user} \
	--password=${from_password} \
	--add-drop-table \
	--add-locks \
	--extended-insert \
	--single-transaction \
	--quick \
	${ignored_tables_string} \
	${from_database} \
	| pv | gzip -c > ${outpath}/${from_database}.sql.gz

echo "Applying on configured database"
zcat \
	${outpath}/${from_database}.sql.gz \
	| pv \
	| mysql \
		--host=${to_host} \
		--user=${to_user} \
		--password=${to_password} \
		${to_database}